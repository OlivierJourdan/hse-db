# hse-db


## :memo: Description 

DB_Connector is a DQMH module that serves as a wrapper for talking to various database systems.

In hse-db the DQMH module for DB connectivity and its database drivers are maintained.


## :rocket: Installation

The latest release version can be found at 
https://dokuwiki.hampel-soft.com/code/open-source/hse-db/releases

Dependencies:
https://dokuwiki.hampel-soft.com/code/open-source/hse-libraries/hse-core-libs
https://dokuwiki.hampel-soft.com/code/open-source/hse-logger
https://dokuwiki.hampel-soft.com/code/open-source/hse-libraries/hse-dqmh

## :bulb: Usage

Please check our DokuWiki for more information
https://dokuwiki.hampel-soft.com/code/open-source/hse-db

## :wrench: Configuration 

Please check our DokuWiki for more information
https://dokuwiki.hampel-soft.com/code/open-source/hse-db

## :busts_in_silhouette: Contributing 

Please contribute! We'd love to see code coming back to this repo. Get in touch if you want to know more about contributing.

##  :beers: Credits

* Joerg Hampel
* Manuel Sebald
* Alexander Elbert

## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details